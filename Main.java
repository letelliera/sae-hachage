import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        String filename = Ut.saisirChaine();
        double f = Ut.saisirDouble();

        Dictionnaire d = new Dictionnaire(filename, f);

        System.out.println("maxSize : " + d.getMaxSize());
        System.out.println("cardinal : " + d.getCardinal());
        System.out.println("nbListes : " + d.getNbListes());

        int nbRecherches = 100000;
        var deb = System.currentTimeMillis();

        for (int i = 0; i < nbRecherches; i++) {
            int tailleMot = random.nextInt(15) + 2; //2 <= tailleMot <= 16
            char[] mot = new char[tailleMot];
            for (int j = 0; j < mot.length; j++) {
                mot[j] = (char) ('a' + random.nextInt(26));
            }
            String motS = new String(mot);
            d.contient(motS);
        }

        var fin = System.currentTimeMillis();
        System.out.println("Temps total: " + (fin-deb));
    }
}
