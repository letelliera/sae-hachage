import java.math.BigInteger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Dictionnaire {

    private HTNaive ht;

    /**
     * Construit un Dictionnaire dont this.ht a m listes vides.
     */
    public Dictionnaire(int m) {
        this.ht = new HTNaive(m);
    }

    /**
     * Construit un dictionnaire dont this.ht a m listes vides, puis ajoute à ht
     * chaque
     * mot différent dans le fichier filename (sous forme de BigInteger et si
     * filename est un chemin valide)
     */
    public Dictionnaire(String filename, int m) {
        this(m);
        this.ht.ajoutListe(calculeListeInt(filename));
    }

    /**
     * Construit un dictionnaire contenant tous les mots différents du fichier
     * filename (sous forme de BigInteger et si filename est un chemin valide),
     * ayant
     * (nombre de mots différents dans filename * f) listes.
     */
    public Dictionnaire(String filename, double f) {
        this.ht = new HTNaive(calculeListeInt(filename), f);
    }

    /**
     * Action: utilise la méthode de Horner
     * 
     * @return une interprétation de s sous la forme d'un entier en base 256, en
     *         base 10
     */
    public static BigInteger stringToBigInteger(String s) {
        BigInteger somme = BigInteger.valueOf(s.charAt(0));

        for (int i = 1; i < s.length(); i++) {
            somme = somme.multiply(BigInteger.valueOf(256)).add(BigInteger.valueOf(s.charAt(i)));
        }
        return somme;
    }

    /**
     * @return true si this.ht contient stringToBigInteger(s)
     */
    public boolean contient(String s) {
        BigInteger u = stringToBigInteger(s);
        return ht.contient(u);
    }

    /**
     * Ajoute stringToBigInteger(s) à this.ht s'il n'est pas déjà présent
     * 
     * @return true si l'ajout est effectué avec succès, false si
     *         stringToBigInteger(s) est déjà présent.
     */
    public boolean ajout(String s) {
        return ht.ajout(stringToBigInteger(s));
    }

    /**
     * @return this.ht.getCardinal();
     */
    public int getCardinal() {
        return ht.getCardinal();
    }

    /**
     * @return this.ht.getMaxSize();
     */
    public int getMaxSize() {
        return ht.getMaxSize();
    }

    /**
     * @return this.ht.getNbListes();
     */
    public int getNbListes() {
        return ht.getNbListes();
    }

    /**
     * @return this.ht.toString();
     */
    public String toString() {
        return ht.toString();
    }

    /**
     * @return this.ht.toStringv2();
     */
    public String toStringv2() {
        return ht.toStringv2();
    }

    /**
     * @return this.ht.toStringv3();
     */
    public String toStringv3() {
        return ht.toStringv3();
    }

    /**
     * @return une ListeBigI contenant tous les mots différents présents dans le
     *         fichier filename (sous forme de BigInteger et si filename est un
     *         chemin valide)
     */
    public static ListeBigI calculeListeInt(String filename) {
        ListeBigI res = new ListeBigI();
        File f = new File(filename);
        Scanner sc;
        try {
            sc = new Scanner(f);
        } catch (FileNotFoundException e) {
            System.out.println("Impossible d'accéder au fichier: " + e.getMessage());
            return res;
        }
        sc.useDelimiter(" |\\n|,|;|:|\\.|!|\\?|-");
        while (sc.hasNext()) {
            String mot = sc.next();
            if (!mot.isEmpty())
                res.ajoutTete(stringToBigInteger(mot));
        }
        sc.close();
        return res;
    }

}
