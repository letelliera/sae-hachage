import java.math.BigInteger;

/**
 * HTNaive
 */
public class HTNaive {

    private ListeBigI[] t;
    private long totalTimeH;
    private long totalTimeContient;

    /**
     * Construit un HTNaive avec m listes vides.
     */
    public HTNaive(int m) {
        this.t = new ListeBigI[m];
        for (int i = 0; i < t.length; i++) {
            t[i] = new ListeBigI();
        }
    }

    /**
     * Construit un HTNaive avec m listes, remplies des éléments de l
     */
    public HTNaive(ListeBigI l, int m) {
        this(m);
        this.ajoutListe(l);
    }

    /**
     * Construit un HTNaive avec (le nombre d'éléments différents de l * f)
     * listes, remplies des éléments de l
     */
    public HTNaive(ListeBigI l, double f) {
        this(l, (int) (new HTNaive(l, 1000).getCardinal() * f));
    }

    /**
     * @return la liste de this ayant l'index i.
     */
    public ListeBigI getListe(int i) {
        return t[i];
    }

    /**
     * @return u % m, m étant le nombre de listes de this.
     *         Ajoute le nombre de millisecondes passées à effectuer la fonction à
     *         this.totalTimeH.
     */
    public int h(BigInteger u) {
        long deb = System.currentTimeMillis();

        BigInteger m = BigInteger.valueOf(t.length);
        int res = u.mod(m).intValue();

        long fin = System.currentTimeMillis();
        totalTimeH += (fin - deb);
        return res;
    }

    /**
     * @return true si u est présent dans l'une des listes de this.
     *         Ajoute le nombre de millisecondes passées à effectuer la fonction à
     *         this.totalTimeContient.
     */
    public boolean contient(BigInteger u) {
        long deb = System.currentTimeMillis();

        int h = h(u);
        ListeBigI liste = t[h];
        boolean res = liste.contient(u);

        long fin = System.currentTimeMillis();
        totalTimeContient += (fin - deb);
        return res;
    }

    /**
     * Ajoute l'élément u s'il n'est pas présent dans this.
     * 
     * @return true si l'élément à été ajouté, false s'il était déjà présent
     */
    public boolean ajout(BigInteger u) {
        int h = h(u);
        ListeBigI liste = t[h];

        if (liste.contient(u)) // utiliser liste.contient() au lieu de this.contient() est légèrement plus
            return false; // rapide car on n'appelle h() qu'une fois.

        liste.ajoutTete(u);
        return true;
    }

    /**
     * Ajoute les éléments de L dans this.
     */
    public void ajoutListe(ListeBigI L) {
        ListeBigI lClone = new ListeBigI(L);

        while (!lClone.estVide()) {
            BigInteger u = lClone.supprTete();
            this.ajout(u);
        }
    }

    /**
     * @return tous les éléments de this, stockés dans une ListeBigI
     */
    public ListeBigI getElements() {
        ListeBigI res = new ListeBigI();
        for (int i = 0; i < t.length; i++) {
            res.ajoutListe(t[i]);
        }
        return res;
    }

    /**
     * @return le nombre de listes de this.
     */
    public int getNbListes() {
        return t.length;
    }

    /**
     * @return le nombre d'éléments de this.
     */
    public int getCardinal() {
        int res = 0;
        for (int i = 0; i < t.length; i++) {
            res += t[i].longueur();
        }
        return res;
    }

    /**
     * @return la longueur de la plus longue liste de this.
     */
    public int getMaxSize() {
        int res = 0;
        for (int i = 0; i < t.length; i++) {
            int length = t[i].longueur();
            res = length > res ? length : res;
        }
        return res;
    }

    /**
     * @return une représentation de this sous forme de String, montrant le nombre
     *         d'éléments de chaque liste non vide sous la forme d'une répétition du
     *         caractère '*'.
     */
    public String toStringv2() {
        String res = "";
        for (int i = 0; i < t.length; i++) {
            if (t[i].estVide())
                continue;

            res += "t[" + i + "]: ";
            res += Ut.repeatChar('*', t[i].longueur());
            res += "\n";
        }
        return res;
    }

    /**
     * @return une représentation de this sous forme de String, montrant le nombre
     *         d'éléments de chaque liste non vide.
     */
    public String toStringv3() {
        String res = "";
        for (int i = 0; i < t.length; i++) {
            if (t[i].estVide())
                continue;

            res += "t[" + i + "]: ";
            res += t[i].longueur() + "\n";
        }
        return res;
    }

    /**
     * @return une représentation de this sous forme de String, montrant tous les
     *         éléments de toutes les listes de this.
     */
    public String toString() {

        String res = "";
        for (int i = 0; i < t.length; i++) {
            res += "t[" + i + "]: " + t[i] + "\n";
        }
        return res;

    }

    /**
     * @return le nombre de millisecondes passées au total à effectuer la fonction
     *         h()
     */
    public long getTotalTimeH() {
        return totalTimeH;
    }

    /**
     * @return le nombre de millisecondes passées au total à effectuer la fonction
     *         contient()
     */
    public long getTotalTimeContient() {
        return totalTimeContient;
    }

}