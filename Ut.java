
// dmaj : fin octobre 2020

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("all")
public class Ut {

	public static void afficher(String ch) {
		System.out.print(ch);
	}

	public static void afficherSL(String ch) {
		afficher(ch);
		passerLigne();
	}

	public static void afficher(int nb) {
		System.out.print(nb + "");
	}

	public static void afficherSL(int nb) {
		afficher(nb);
		passerLigne();
	}

	/** Retour a la ligne : */

	public static void passerLigne() {
		System.out.println("");
	}

	public static void passerALaLigne() {
		passerLigne();
	}

	public static void sauterALaLigne() {
		passerLigne();
	}

	public static void sautLigne() {
		passerLigne();
	}

	public static void afficher(double nb) {
		System.out.print(nb + "");
	}

	public static void afficherSL(double nb) {
		afficher(nb);
		passerLigne();
	}

	public static void afficher(float nb) {
		System.out.print(nb + "");
	}

	public static void afficherSL(float nb) {
		afficher(nb);
		passerLigne();
	}

	public static void afficher(char c) {
		System.out.print(c + "");
	}

	public static void afficherSL(char c) {
		afficher(c);
		passerLigne();
	}

	public static void afficher(boolean b) {
		System.out.print(b + "");
	}

	public static void afficherSL(boolean b) {
		afficher(b);
		passerLigne();
	}

	public static void afficher(int[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				System.out.print(mat[i][j] + "\t");
			}
			sautLigne();
		}
	}

	public static void afficherSL(int[][] mat) {
		afficher(mat);
		passerLigne();
	}

	public static int saisirEntier() {

		Scanner clavier = new Scanner(System.in);
		String s = clavier.nextLine(); // int lu = clavier.nextInt();
		int lu = 456;
		try {
			lu = Integer.parseInt(s);
		} catch (NumberFormatException ex) {
			System.out.println("Ce n'est pas un entier valide");
		}
		return lu;
	}

	public static double saisirDouble() {
		return saisirReel();
	}

	public static double saisirReel() {

		Scanner clavier = new Scanner(System.in);
		String s = clavier.nextLine();
		double lu = -123.987;
		try {
			lu = Double.parseDouble(s);
		} catch (NumberFormatException ex) {
			System.out.println("Ce n'est pas un reel valide");
		}
		return lu;
	}

	public static float saisirFlottant() {

		Scanner clavier = new Scanner(System.in);
		String s = clavier.nextLine();
		float lu = -123.987F;
		try {
			lu = Float.parseFloat(s);
		} catch (NumberFormatException ex) {
			System.out.println("Ce n'est pas un reel valide");
		}
		return lu;
	}

	public static char saisirCaractere() {

		Scanner clavier = new Scanner(System.in);
		char lu = clavier.next().charAt(0);
		return lu;
	}

	public static boolean saisirBooleen() {

		Scanner clavier = new Scanner(System.in);
		boolean lu = clavier.nextBoolean();
		return lu;
	}

	public static String saisirChaine() {

		Scanner clavier = new Scanner(System.in);
		String s = clavier.nextLine();
		return s;
	}

	public static char[] saisirCharArray() {

		Scanner clavier = new Scanner(System.in);
		String s = clavier.nextLine();
		char[] tab = s.toCharArray();
		return tab;
	}

	public static int alphaToIndex(char c) {
		// Prerequis : c est un caractere entre 'a' et 'z'
		// Resultat : la valeur 0 pour 'a', 12 pour 'm', 25 pour 'z'...
		return (int) c - 97;
	}

	public static char indexToAlpha(int i) {
		// Prerequis : i est un entier entre 0 et 25
		// (par exemple, indice d'un tableau)
		// Resultat : la valeur 'a' pour 0, 'm' pour 12, 'z' pour 25...b
		return (char) (i + 97);
	}

	/**
	 * @param a entier
	 * @param b entier Pre-requis : aucun
	 * @return le PGCD de a et b en utilisant l'algorithme d'Euclide
	 */
	public static int pgcd(int a, int b) {

		a = Math.abs(a);
		b = Math.abs(b);
		int temp;
		while (b != 0) {
			temp = b;
			b = a % b;
			a = temp;
		}
		return a;
	}

	public static int randomMinMax(int min, int max) {
		// Resultat : un entier entre min et max choisi aleatoirement
		Random rand = new Random();
		int res = rand.nextInt(max - min + 1) + min;
		// System.out.println(res + " in [" + min + "," + max + "]");
		// assert min <= res && res <= max : "tirage aleatoire hors des bornes";
		return res;
	}

	public static void clearConsole() {
		// Action : efface la console (le terminal)
		System.out.print("\033[H\033[2J");
	}

	public static void pause(int timeMilli) {
		// Action : suspend le processus courant pendant timeMilli millisecondes
		try {
			Thread.sleep(timeMilli);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}

	public static boolean estEntier(double n) {
		// Resultat : vrai ssi n est un reel qui est aussi un entier (exemple: 5.0)
		return (int) n == n;
	}

	public static int modulo2(int a, int b) {
		// Pré-requis : b != 0 mais b peut être négatif
		// Résultat : a modulo b, un nombre entre 0 et b-1 si b est positif, entre b+1
		// et 0 sinon.
		// Suit la définition : a mod b := a - b * E(a/b)
		// Remarque : l'opérateur modulo de Java ne suit pas cette définition et
		// peut rendre un nombre de signe opposé à b.
		return a - b * (int) Math.floor((double) a / b);
	}

	public static boolean estBissextile(int an) {
		// Resultat : vrai ssi an est bissextile
		return an % 400 == 0 || an % 4 == 0 && an % 100 != 0;
	}

	/**
	 * @return un String, la répétition de c, i fois.
	 */
	public static String repeatChar(char c, int i) {
		String s = "";
		for (int j = 0; j < i; j++) {
			s = s + c;
		}
		return s;
	}

	/**
	 * @return un String, la répétition de s, i fois.
	 */
	public static String repeatString(String s, int i) {
		String res = "";

		for (int j = 0; j < i; j++) {
			res = res + s;
		}
		return res;
	}

	/**
	 * @param c est une lettre de [a-z] ou [A-Z]
	 * @return le numéro de c. A = 0, B = 1, C = 2, etc.
	 */
	public static int jetonToInt(char c) {
		char cLower = Character.toLowerCase(c);
		return Ut.alphaToIndex(cLower);
	}

	public static char intToJeton(int n) {
		char cLower = Ut.indexToAlpha(n);
		return Character.toUpperCase(cLower);
	}

	/**
	 * Concatène 2 int[]
	 * 
	 * @return un int[] de longueur ar1.length + ar2.length, contenant les éléments
	 *         de ar2 à la suite de ceux de ar1
	 */
	public static int[] concatIntArrays(int[] ar1, int[] ar2) {
		int l1 = ar1.length;
		int l2 = ar2.length;
		int[] res = new int[l1 + l2];

		for (int i = 0; i < l1; i++) {
			res[i] = ar1[i];
		}
		for (int i = l1; i < l1 + l2; i++) {
			res[i] = ar2[i - l1];
		}

		return res;
	}

	/**
	 * stratégie: Regex
	 * 
	 * @return true si s ne contient que des lettres entre A et Z.
	 */
	public static boolean stringEntreAetZ(String s) {
		Pattern p = Pattern.compile("[A-Z]+");
		Matcher m = p.matcher(s);
		m.find();
		return m.matches();
	}

	/**
	 * @return
	 */
	public static boolean positionValide(String s) {
		Pattern p = Pattern.compile("[A-O]\\d\\d?");
		Matcher m = p.matcher(s);
		m.find();
		if (m.matches()) {
			int n = Integer.parseInt(s.substring(1));
			if (n >= 1 && n <= 15)
				return true;
		}
		return false;
	}

	/**
	 * réalise une symétrie par l'axe diagonal \ de la matrice. Les éléments en haut
	 * à droite de l'axe seront copiés en bas à droite. Les éléments sur l'axe
	 * seront intouchés. Ne change pas le paramètre triangle.
	 * 
	 * @param triangle : int[][] carré
	 * @return triangle, symétrisé par sa diagonale.
	 */
	public static int[][] symAxeDiagonal(int[][] triangle) {
		int[][] res = triangle.clone();
		for (int y = 0; y < res.length; y++) {

			for (int x = 0; x < res.length; x++) {

				if (y > x) {
					res[y][x] = res[x][y];
				}

			}

		}
		return res;
	}

	/**
	 * réalise une symétrie en prenant la dernière colonne de input en tant qu'axe
	 * de symétrie. Les éléments de gauche seront copiés à droite de l'axe. Les
	 * éléments sur l'axe seront intouchés. Ne change pas le paramètre input.
	 * 
	 * @param input int[][]
	 * @return un int[][] de taille {input.length, input[0].length * 2 - 1}, doublant la "longueur" de input, et symétrisant ses éléments
	 */
	public static int[][] symAxeVertical(int[][] input) {
		int[][] output = new int[input.length][input[0].length * 2 - 1];
		int bigX = input[0].length * 2 - 2;
		for (int y = 0; y < output.length; y++) {

			for (int x = 0; x < output[0].length; x++) {
				if (x < input[0].length) {
					output[y][x] = input[y][x];
				} else {
					output[y][x] = input[y][bigX - x];

				}

			}

		}

		return output;
	}

	/**
	 * réalise une symétrie en prenant la dernière ligne de input en tant qu'axe
	 * de symétrie. Les éléments du haut seront copiés en bas de l'axe. Les
	 * éléments sur l'axe seront intouchés. Ne change pas le paramètre input.
	 * 
	 * @param input int[][]
	 * @return un int[][] de taille {input.length * 2 - 1, input[0].length}, doublant la "hauteur" de input, et symétrisant ses éléments
	 */
	public static int[][] symAxeHorizontal(int[][] input) {
		int[][] output = new int[input.length * 2 - 1][input[0].length];
		int bigY = input.length * 2 - 2;
		for (int y = 0; y < output.length; y++) {

			for (int x = 0; x < output[0].length; x++) {

				if (y < input.length) {
					output[y][x] = input[y][x];
				} else {
					output[y][x] = input[bigY - y][x];
				}

			}

		}

		return output;
	}
}
// end class